import urllib, urllib2, urlparse, sys
import xml.etree.ElementTree as ET
from gamesdb.api import API, urlencode_no_plus

class GDBAPI(API):
    
    def get_art(self, id=None, name=None, platform=None):
        """
        returns game_art struct
        game_art['boxart']['front']
        game_art['boxart']['back']
        
        """
        if id is not None and name is not None:  # One of these arguments must be passed
                return None
        else:
            query_args = {}
            if id is not None:
                query_args['id'] = id
            if name is not None:
                query_args['name'] = name
            if platform is not None:
                query_args['platform'] = platform

        GET_PLATFORM_GAMES_LIST_ENDPOINT = 'http://thegamesdb.net/api/GetArt.php?'
        xml_response = self.make_call(GET_PLATFORM_GAMES_LIST_ENDPOINT, query_args)
        
        game_art = {}
        game_art['boxart'] = []
        game_art['banner'] = []
        game_art['screenshot'] = []
        base_url = xml_response.find("baseImgUrl").text
        # print base_url
        for element in xml_response.iter(tag="Images"):
            for subelement in element:
                if subelement.tag == 'boxart':
                    boxart ={}
                    boxart['width'] = subelement.attrib.get('width')
                    boxart['height'] = subelement.attrib.get('height')
                    boxart['side'] = subelement.attrib.get('side')
                    boxart['img_url'] = urlparse.urljoin(base_url, subelement.text)
                    game_art['boxart'].append(boxart)
                    
                elif subelement.tag == 'banner':
                    banner = {}
                    banner['width'] = subelement.attrib.get('width')
                    banner['height'] = subelement.attrib.get('height')
                    banner['img_url'] = urlparse.urljoin(base_url, subelement.text)
                    game_art['banner'].append(banner)
                elif subelement.tag == 'screenshot':
                    sshot = {}
                    # sshots have .original and .thumb
                    for sub in subelement:
                        if sub.tag == 'original':
                            
                            sshot['width'] = sub.attrib.get('width')
                            sshot['height'] = sub.attrib.get('height')
                            sshot['img_url'] = urlparse.urljoin(base_url, sub.text)
                            game_art['screenshot'].append(sshot)
                else:
                    # print subelement.tag
                    pass

        
        
        return game_art
        
    
    @staticmethod
    def make_call(api_url, query_args=None):
        # api_url is expected to be the fully constructed URL, with any needed arguments appended.
        # This function will simply make the call, and return the response as an ElementTree object for parsing,
        # If response cannot be parsed because it is not valid XML, this function assumes an API error and raises an
        # APIException, passing forward the pages contents (which generally gives some indication of the error.
        
        # in order to fix cloudflare issue, I'll be creating a Request object
        # with proper headers built into it...
        
        opener = urllib2.build_opener() 
        
        if query_args is not None:
            get_params = urlencode_no_plus.urlencode_no_plus(query_args)

            # response = urllib.urlopen(api_url+'%s' % get_params)
            api_url = api_url + '%s' % get_params
        # else:
        #    response = urllib.urlopen(api_url)
        
        request = urllib2.Request(api_url)
        request.add_unredirected_header('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.64 Safari/537.31')
        response = urllib2.urlopen(request)
        page = response.read()

        # Make sure the XML Parser doesn't return a ParsError.  If it does, it's probably and API Issue, so raise an
        # exception, printing the response from the API call.
        try:
            xml_response = ET.fromstring(page)
        except ET.ParseError:
            raise APIException(page)
        return  xml_response
        
def main():
    # test the gamedb connection
    gamesdb_api = GDBAPI()
    # platform_list = gamesdb_api.get_platforms_list()
    game_name = " ".join(sys.argv[1:])
    get_by_name(gamesdb_api, game_name)
    
def get_by_name(gamesdb_api, game_name):
    """docstring for get_candidates"""
    glist = gamesdb_api.get_game(name=game_name)
    # for game in glist:
    #     print game.title
    #     print game.platform
    #     print gamesdb_api.get_art(id=game.id)
    #     print "---------------------"
    glist or sys.exit()
    for game in glist:
        game.art=gamesdb_api.get_art(id=game.id)
        print game.title, game.art['screenshot']


if __name__ == '__main__':
    main()